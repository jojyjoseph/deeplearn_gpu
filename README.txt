Description : Deeplearing implementation of image recognition on NVIDIA GK20 GPU.

nvcc path to be mentioned in Makefile

All Cuda development to be found in convolutionSeperable.cu

# README #

The source file is a solo file for the scheduler to be implemented on Real Time extension of Linux, LITMUS.
### What is this repository for? ###

* Deeplearning is a machine learning branch. It is used for image recognition and involves massive parallel computations.
* Repository contains Deeplearning L1, L2 stage implementations for NVIDIA GPU. 

### How do I get set up? ###

* Source code available from the repository.
* Modify the Makefile for correct path of NVCC, Cuda compiler

### Contribution guidelines ###

* Cuda source code development
* Code review and tests
* Project Report

### Who do I talk to? ###

* George Joseph