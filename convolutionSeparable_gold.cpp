/*
 * Copyright 1993-2014 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */


#include "convolutionSeparable_common.h"



////////////////////////////////////////////////////////////////////////////////
// Reference row convolution filter
////////////////////////////////////////////////////////////////////////////////
extern "C" void convolutionRowCPU(
    float *h_Dst,
    float *h_Src,
    float *h_Kernel,
    int imageW,
    int imageH,
    int kernelR
)
{
    for (int y = 0; y < imageH; y++)
        for (int x = 0; x < imageW; x++)
        {
            float sum = 0;

            for (int k = -kernelR; k <= kernelR; k++)
            {
                int d = x + k;

                if (d >= 0 && d < imageW)
                    sum += h_Src[y * imageW + d] * h_Kernel[kernelR - k];
            }

            h_Dst[y * imageW + x] = sum;
        }
}



////////////////////////////////////////////////////////////////////////////////
// Reference column convolution filter
////////////////////////////////////////////////////////////////////////////////
extern "C" void convolutionColumnCPU(
    float *h_Dst,
    float *h_Src,
    float *h_Kernel,
    int imageW,
    int imageH,
    int kernelR
)
{
    for (int y = 0; y < imageH; y++)
        for (int x = 0; x < imageW; x++)
        {
            float sum = 0;

            for (int k = -kernelR; k <= kernelR; k++)
            {
                int d = y + k;

                if (d >= 0 && d < imageH)
                    sum += h_Src[d * imageW + x] * h_Kernel[kernelR - k];
            }

            h_Dst[y * imageW + x] = sum;
        }
}

static int get_index_arm(
		int row,
		int col,
		int max_cols
		)
{
	return (max_cols*row + col);
}	

static int get_src_value(
		int row,
		int col,
		int max_rows,
		int max_cols,
		float *h_kernel
		)
{
	if(row<0 || col<0 || row>=max_rows || col>=max_cols)
		return h_kernel[get_index_arm(row, col,max_rows)];
	else
		return 0;
}

extern "C" void convolution_arm(
		float *h_dst,
		float *h_src,
		float *h_kernel,
		int max_rows,
		int max_cols,
		int kernelr
		)
{
	for(int l=0;l<max_rows;l++)
		for(int m=0;m<max_cols;m++)
			for(int i=-kernelr;i<=kernelr;i++)
				for(int j=-kernelr;j<=kernelr;j++)
					h_dst[get_index_arm(l,m,max_cols)]=get_src_value(l,m,max_rows,max_cols,h_kernel)*(h_kernel[(i+kernelr)*(2*kernelr +1)+j]);

}
