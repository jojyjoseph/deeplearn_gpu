/*

   *------------------------------------------------------
   *		READ COMMENTS FOR GUIDELINES
   *------------------------------------------------------
   */


//	following header files taken from teradeep_main.cpp
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
//following header files taken from main.cpp(convolutionSeperable)
	// CUDA runtime
#include <cuda_runtime.h>
	// Utilities and system includes
#include <helper_functions.h>
#include <helper_cuda.h>
//following header files taken from teradeep_main.cpp
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/gpu/gpu.hpp>
#include "classify.h"

#include"convolutionSeparable_common.h"


/*
   *	File:main.cpp		Description:program starts here
   *	
   *	----------------timing measurement------------------
   *	variable:StopWatchInterface		parameter(1st):htimer
   *	functions to deal with timer variable(self explaintory)
   *	sdkCreateTimer(),sdkStopTimer(),sdkResetTimer(),sdkGetTimerValue(), sdkDeleteTimer()
   *	---------------------------------------------------
   */

    
int main(int argc, char **argv)
{
	// DP3647 - variables declared to manage argc, argv
	
	long start_time,stop_time,L1_stop,L2_start,L2_stop,L3_start,L3_stop,classify_time;
	int max_rows, max_cols,row,col,num;
	int L1_MAPS , L2_MAPS , L3_MAPS ;
	int L1_KERNEL_SIZE , L2_KERNEL_SIZE , L3_KERNEL_SIZE ;
	int L1_L2_NO_CONNECTIONS , L2_L3_NO_CONNECTIONS ;
	int NO_INPUT_NEURONS , NO_HIDDEN_NEURONS , NO_OUTPUT_NEURONS ;
	

	//-----------------------------------------------------------------------

	max_rows = atoi(argv[1]);
	max_cols = atoi(argv[2]);
	L1_MAPS = atoi (argv[3]);
	L2_MAPS = atoi (argv[4]);
	L3_MAPS = atoi(argv[5]);
	L1_KERNEL_SIZE = atoi(argv[6]);
	L2_KERNEL_SIZE = atoi(argv[7]);
	L3_KERNEL_SIZE = atoi(argv[8]);
	L1_L2_NO_CONNECTIONS = atoi(argv[9]);
	L2_L3_NO_CONNECTIONS = atoi(argv[10]);
	NO_HIDDEN_NEURONS = atoi(argv[11]);
	NO_OUTPUT_NEURONS = atoi(argv[12]);
	NO_INPUT_NEURONS = L3_MAPS*(max_rows/8)*(max_cols/8) ;
	//-------------------------------------------------------------------
	float * input_weights = (float *) malloc(NO_INPUT_NEURONS*sizeof(float));
	float * hidden_weights = (float *) malloc(NO_HIDDEN_NEURONS*sizeof(float));
	float * output_weights = (float *) malloc(NO_OUTPUT_NEURONS*sizeof(float));
	float * input_bias = (float *) malloc(NO_INPUT_NEURONS*sizeof(float));
	float * hidden_bias = (float *) malloc(NO_HIDDEN_NEURONS*sizeof(float));
	float * output_bias = (float *) malloc(NO_OUTPUT_NEURONS*sizeof(float));
	float * neuron_output = (float *) malloc(NO_OUTPUT_NEURONS*sizeof(float));
	uint8_t classifier_input[NO_INPUT_NEURONS];


	//MNIST INPUT
		//MNIST Test
	float *input = (float *)malloc(28*28*sizeof(float));
	float *label = (float *)malloc(10*sizeof(float));
	char filetrain[] = "train-images.idx3-ubyte";
	char filelabel[] = "train-labels.idx1-ubyte";
	unsigned char temp;	
	float lab;
	int i,k,j, magic_number, number_of_images, n_rows, n_cols;

	
	FILE *f, *g;
	f = fopen(filetrain,"rb");
	if (!f)
	{
		printf("Unable to open file!");
		return 1;
	}

	g = fopen(filelabel,"rb");
	if (!f)
	{
		printf("Unable to open file!");
		return 1;
	}
	//Reading headers image file 
	fread(&magic_number, sizeof(magic_number),1,f);
	magic_number = ReverseInt(magic_number);
	fread(&number_of_images, sizeof(number_of_images),1,f);
	number_of_images = ReverseInt(number_of_images);
	fread(&n_rows, sizeof(n_rows),1,f);
	n_rows = ReverseInt(n_rows);
	fread(&n_cols, sizeof(n_cols),1,f);
	n_cols = ReverseInt(n_cols);
	//Reading headers label file
	fread(&magic_number, sizeof(magic_number),1,g);
	magic_number = ReverseInt(magic_number);
	fread(&number_of_images, sizeof(number_of_images),1,g);
	number_of_images = ReverseInt(number_of_images);

	for(i = 0;i<28*28 ;i++){
			fread(&temp, sizeof(temp),1,f);
			input[i] = (float)temp;
		}

	//Reading label data		

//	cv::Mat temp_img;
//	temp_img = Mat(28,28,CV_32FC1,input);
//	imwrite("input.png",temp_img);

	fread(&temp, sizeof(temp),1,g);
	lab = (float)temp;
	for(i = 0; i <10; i++){
		label[i] = 0;
	}
	label[(int)lab] = 1;

	fclose(f);
	fclose(g);
	free(input);
	free(label);





	if((L1_L2_NO_CONNECTIONS > L1_MAPS) || (L1_L2_NO_CONNECTIONS <= 0))
	{
		printf("L1_L2_CONNECTION weight %d  is not valid . Range [ 1 , %d]\n" ,L1_L2_NO_CONNECTIONS, L1_MAPS );
		return -1 ;

	}


	if((L2_L3_NO_CONNECTIONS > L2_MAPS) || (L2_L3_NO_CONNECTIONS <= 0))
	{
		printf("L2_L3_CONNECTION weight %d is not valid . Range [ 1 , %d]\n" ,L2_L3_NO_CONNECTIONS, L2_MAPS);
		return -1 ;
	}
	//---------------------------------------------------------
	for ( num = 0 ; num < NO_INPUT_NEURONS ; num++)
	{

		*(input_weights+num) = -1+2*((float)rand())/RAND_MAX ;
		*(input_bias+num) = 0;
	}

	for ( num = 0 ; num < NO_HIDDEN_NEURONS ; num++)
	{
		*(hidden_weights+num) = -1+2*((float)rand())/RAND_MAX ;
		*(hidden_bias+num) = 0;
	}

	for ( num = 0 ; num < NO_OUTPUT_NEURONS ; num++)
	{
		*(output_weights+num) = -1+2*((float)rand())/RAND_MAX ;
		*(output_bias+num) = 0;
	}
	//-------------------------------------------------------------------------------
	
	// DP3647 -- ends
	
	/*
	   *	L1 layer computations to be done here
	   *	function: compute_l1()
	   *	description	: computes filter2d, maxpooling(resize+dilate) on image by a single kernel
	   *	To begin operations, 
	   *	1) "L1_MAPS" nos of filter kernels have to be initialized in constant memory
	   *	2) Device_image (image stored in global memory)has to be in multiples of "Shared image block" going to be put in shared memory
	   *	3) Device_image has to be inclusive of ghost image, which is determined by "kernel_length"
	   *	parameters :
	   *
	   */


	/*
	*	FILTER KERNEL INITIALIZATION for L1_MAPS
	*/	
	
	float h_Kernel[49*256];	//host memory allocated
	for (unsigned int i = 0; i < (49*256); i++)	// randomly value assigned
        {
	        h_Kernel[i] = 1;//(float)(rand() % 2);
    	}
	
	for (unsigned int i = 49; i < 98; i++)	// randomly value assigned
        {
//	        h_Kernel[i] = 2;//(float)(rand() % 2);
    	}

	for (unsigned int i = 98; i < 147; i++)	// randomly value assigned
        {
	        h_Kernel[i] = 1;//(float)(rand() % 2);
    	}
	setConvolutionKernel_dl(h_Kernel,L1_MAPS);	//kernel values transferred to device

	float h_Kernel_l2[128*25];	//host memory allocated
	for (unsigned int i = 0; i < (128*25); i++)	// randomly value assigned
        {
	        h_Kernel_l2[i] = 1;//(float)(rand() % 2);
    	}
	
	for (unsigned int i = 25; i < 50; i++)	// randomly value assigned
        {
//	        h_Kernel_l2[i] = 2;//(float)(rand() % 2);
    	}

	for (unsigned int i = 50; i < 75; i++)	// randomly value assigned
        {
	        h_Kernel_l2[i] = 1;//(float)(rand() % 2);
    	}
	setConvolutionKernel_dl_l2(h_Kernel_l2,L1_MAPS);	//kernel values transferred to device



	//device pointers
	float *d_input,
		*dt_input,
	      *d_output_l1,
		*dc_output,
		*ds_image,
		*d_inter_l2,
		*d_output_l2,
	      *d_output;
	int *d_rand_l1_l2;
	//host pointers
	float	*h_orig,	//original image 
		*h_input,	//image with ghost 
	      *ht_output,	//test array
		*hc_output,
		*hs_image,
		*h_inter_l2,
	      *h_output;	//final output image
	int *h_l1_l2,
	    *ht_l1_l2;	
	// Quantum of 32's
	int n_row,n_col;
	n_row=(31 + max_cols)/32;
	n_col=(31 + max_rows)/32;
	
	//IMAGE CREATION	
	h_orig		=(float *)malloc((max_rows) * (max_cols) * sizeof(float)); // 	image allocation
	
	//image data filling
	for (unsigned int i = 0; i < max_rows; i++)	// randomly value assigned
        {
	        for(unsigned int j = 0; j < max_cols; j++)
		{
			h_orig[(max_cols)*i + j] = 1;//(float)(rand() % 16);
		}
    	}
	
	//Image recreated in Quantum of 32's
	h_input		=(float *)malloc(max_rows * max_cols * sizeof(float)); //image with ghost
	h_output	=(float *)malloc(max_rows * max_cols * sizeof(float));
	
	ht_output	=(float*)malloc(L1_MAPS*max_rows/2*max_cols/2*sizeof(float));
	hc_output	=(float*)malloc(max_rows*max_cols*sizeof(float));
	hs_image	=(float*)malloc(7*7*sizeof(float));
	h_inter_l2	=(float*)malloc(L2_MAPS*(max_rows/2)*(max_cols/2)*sizeof(float));
	h_l1_l2		=(int*)	malloc(	(L1_L2_NO_CONNECTIONS + 1)*L2_MAPS * sizeof(int));
	ht_l1_l2	=(int*)	malloc(	(L1_L2_NO_CONNECTIONS + 1)*L2_MAPS * sizeof(int));

	//L1_L2 connection creation
	for(int k=0;k< ((L2_MAPS * (L1_L2_NO_CONNECTIONS + 1))) ; k++)
	{
		h_l1_l2[k]=rand() % 50;
	}
	for(int k=0;k<11;k++)
	{
//		h_l1_l2[k]=1;
	}
	
	for(int k=0;k< ((L2_MAPS * (L1_L2_NO_CONNECTIONS + 1))) ; k++)
	{
		printf("%d ",h_l1_l2[k]);//rand() % 50;
	}



	//setting the image(h_orig) in h_input 
	printf("Allocating and initializing CUDA arrays...\n");
	//L1 memory allocations
        checkCudaErrors(cudaMalloc((void **)&d_input	,(max_rows) * (max_cols) * sizeof(float)));
	checkCudaErrors(cudaMalloc((void **)&d_output_l1,  (L1_MAPS*max_rows/2 * max_cols/2 * sizeof(float))));
	//L2 cuda memory allocations
	checkCudaErrors(cudaMalloc((void **)&d_inter_l2,  (L2_MAPS*max_rows/2 * max_cols/2 * sizeof(float))));
	checkCudaErrors(cudaMalloc((void **)&d_rand_l1_l2,  (L1_L2_NO_CONNECTIONS + 1)*L2_MAPS * sizeof(int)));
	checkCudaErrors(cudaMalloc((void **)&d_output_l2,  (L2_MAPS*max_rows/4 * max_cols/4 * sizeof(float))));


	checkCudaErrors(cudaMemcpy(d_input, h_orig, (max_rows) * (max_cols) * sizeof(float), cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(d_rand_l1_l2, h_l1_l2,  (L1_L2_NO_CONNECTIONS + 1)*L2_MAPS * sizeof(int), cudaMemcpyHostToDevice));
       

//test
float *htt=(float *) malloc( L2_MAPS * max_rows/4 * max_cols/4 *sizeof(float));
float *httt=(float *) malloc( L2_MAPS * max_rows/4 * max_cols/4 *sizeof(float));

//test ends	

	operation_L1(
		d_input,
		d_output_l1,
		max_rows,
		max_cols,
		L1_KERNEL_SIZE,
		L1_MAPS
		);
	operation_L2(
		d_output_l1, 
		d_inter_l2,
		d_output_l2,
		d_rand_l1_l2,
		max_rows,
		max_cols,
		L1_L2_NO_CONNECTIONS,
		L2_MAPS,
		L2_KERNEL_SIZE
		);


	
	checkCudaErrors(cudaMemcpy(ht_output, d_output_l1, L1_MAPS * (max_rows/2) * (max_cols/2) * sizeof(float), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(h_inter_l2, d_inter_l2, L2_MAPS * (max_rows/2) * (max_cols/2) * sizeof(float), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(ht_l1_l2, d_rand_l1_l2, L2_MAPS * (L1_L2_NO_CONNECTIONS + 1) * sizeof(int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(htt, d_output_l2, L2_MAPS*max_rows/4*max_cols/4* sizeof(float), cudaMemcpyDeviceToHost));

	for(int q=0;q<L2_MAPS;q++)
	{
		for(int w=0;w<max_cols/4;w++)
		{
			for(int e=0;e<max_rows/4;e++)
			{
//				httt[q*(max_rows/4)*(max_cols/4) + w*max_cols/4 +e ] = htt[q*32*32 + w*max_cols/4 + e];
			}
		}
	}
	
	printf("h_output \n");
	for(int map=0;map<L2_MAPS;map++)
	{
		printf("7X7_image %d\n",map);
	for(int l=0;l<max_cols/4;l++)
	{
		for(int m=0;m<max_cols/4;m++)
			{
			printf("%1.2f ",htt[map*32 +l*7 + m]);
			}
		printf("\n");
	}
	}


/*
	printf("h_output \n");
	for(int map=0;map<L2_MAPS;map++)
	{
		printf("asdf %d\n",map);
	for(int l=0;l<32;l++)
	{
		for(int m=0;m<32;m++)
			{
			printf("%1.2f ",htt[l*32 + m]);
			}
		printf("\n");
	}
	}
*/
// main test script
/*	for(int map=0;map<L1_MAPS;map++)
{
	printf("ht_output %d\n",map);
	for(int l=0;l<max_rows/2;l++)
	{
		for(int m=0;m<max_cols/2;m++)
			{
			printf("%1.2f ",ht_output[map*(max_rows/2)*(max_cols/2) + max_cols/2*l + m]);
			}
		printf("\n");
	}
}
// L2 inter value
	for(int map=0;map<L2_MAPS;map++)
{	
	printf("\nL2_maps: %d ,Random mapping for map %d: ",L2_MAPS,map);
	for(int k=0;k<L1_L2_NO_CONNECTIONS + 1;k++)
		printf("%d ",ht_l1_l2[map*(L1_L2_NO_CONNECTIONS +  1) + k]);
	printf("\nL2 inter %d\n",map);
	for(int l=0;l<max_rows/2;l++)
	{
		for(int m=0;m<max_cols/2;m++)
			{
			printf("%1.2f ",h_inter_l2[map*(max_rows/2)*(max_cols/2) + max_cols/2*l + m]);
			}
		printf("\n");
	}

}
*/

	free(h_input);
	free(h_output);
	free(ht_output);
	cudaFree(d_input);
	cudaFree(dt_input);
	exit(EXIT_SUCCESS);
}
