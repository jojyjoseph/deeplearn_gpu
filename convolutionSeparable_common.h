/*
 * Copyright 1993-2014 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */



#ifndef CONVOLUTIONSEPARABLE_COMMON_H
#define CONVOLUTIONSEPARABLE_COMMON_H



#define NO_CONSTANT_MAPS	256
#define KERNEL_WINDOW_SIZE	9
#define KERNEL_RADIUS		1

////////////////////////////////////////////////////////////////////////////////
//	DEEP LEARNING
////////////////////////////////////////////////////////////////////////////////
extern "C" void setConvolutionKernel_dl(float *h_Kernel,int L1_MAPS);
extern "C" void setConvolutionKernel_dl_l2(float *h_Kernel_l2,int L2_MAPS);


//extern "C" void	operation_L1(float *d_input,float *d_output ,int max_rows, int max_cols,int kernelr);
//	extern "C" void	operation_L1(float  *d_input,float *d_output ,int image_width,int kernelr,int l1_maps);
extern "C" void	operation_L1(float  *d_input, float *d_output_l1, int max_rows, int max_cols,int kernel_length, int l1_maps);

extern "C" void	operation_L2(float  *d_output_l1, float *d_inter_l2, float *d_output_l2, int *d_rand_l1_l2, int max_rows, int max_cols, int l1_l2_connections, int l2_maps, int kernel_length);


extern "C" void convolution_arm(
		float *h_dst,
		float *h_src,
		float *h_kernel,
		int max_rows,
		int max_cols,
		int kernelr
		);

#endif
