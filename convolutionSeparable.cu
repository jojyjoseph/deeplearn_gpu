/*
 * Copyright 1993-2014 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */



#include <assert.h>
#include <helper_cuda.h>
#include "convolutionSeparable_common.h"
#include <sys/time.h>
__constant__ float d_Kernel[256*49];	//assigning kernel filter to constant memory
__constant__ float d_Kernel_l2[128*25];	//assigning fiter for convolution for L2
//	Cuda Transfer of Fiter kernel for L1_MAPS from host to Constant memory
extern "C" void setConvolutionKernel_dl(float *h_Kernel,int L1_MAPS)	
{
    cudaMemcpyToSymbol(d_Kernel, h_Kernel,256*49* sizeof(float));
}
extern "C" void setConvolutionKernel_dl_l2(float *h_Kernel_l2,int L2_MAPS)	
{
    cudaMemcpyToSymbol(d_Kernel_l2, h_Kernel_l2,25*128* sizeof(float));
}

__device__ float get_image_value_kernel_l1(
		int x,
		int y,
		int l,
		int m,
		int kernel_length,
		int image_width,
		int image_height,
		float *d_Kernel
		)
{
	if(x>=0 && x<image_width && y>=0 && y<image_height)
		return d_Kernel[m*kernel_length +l];
	else 
		return 1;
}

__device__ float get_image_value(
		int x,
		int y,
		int image_width,
		int image_height,
		float *d_input
		)
{
	if(x>=0 && x<image_width && y>=0 && y<image_height)
		return d_input[y*image_width +x];
	else 
		return 0;
}

__device__ float get_image_value_l2(
		int x,
		int y,
		int l2_image_width,
		int l2_image_height,
		int map,
		int l2_inter_image_size,
		float *d_input
		)
{
	if(x>=0 && x<l2_image_width && y>=0 && y<l2_image_height)
		return d_input[l2_inter_image_size*map + y*l2_image_width +x];
	else 
		return 0;
}

//Kernel 1 : filter2d for L1
__global__ void filter2d(
		float *d_input,
		float *d_output_l1,
		int image_height,
		int image_width,
		int kernel_length,
		int l1_map,
		int l1_image_size
		)
{
	//Thread id
	int x=threadIdx.x + blockDim.x*blockIdx.x;
	int y=threadIdx.y + blockDim.y*blockIdx.y;

	//allocate shared memory
	__shared__ float s_image[38][38];
	__shared__ float s_map_image[32][32];
//	__shared__ float wt;
	int kernel_size=kernel_length*kernel_length;
	int kernel_radius=(kernel_length)/2;
	int l1_image_width=image_width/2;

	if(x>=0 && x<image_width && y>=0 && y<image_height)
	{

		s_image[threadIdx.x][threadIdx.y]=0;
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y]=0;
		__syncthreads();
		s_image[threadIdx.x][threadIdx.y+6]=0;
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y+6]=0;
		__syncthreads();

		s_map_image[threadIdx.x][threadIdx.y]=0;
		__syncthreads();
		//copy block to shared memory - ghost included
		s_image[threadIdx.x][threadIdx.y]=get_image_value(x-3,y-3,image_width,image_height,d_input);
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y]=get_image_value(x+3,y-3,image_width,image_height,d_input);
		__syncthreads();
		s_image[threadIdx.x][threadIdx.y+6]=get_image_value(x-3,y+3,image_width,image_height,d_input);
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y+6]=get_image_value(x+3,y+3,image_width,image_height,d_input);
		__syncthreads();

		for(int map=0;map<l1_map;map++)
		{
			//Convolution
//			float wt=0.0;
			int wt=0;
			for(int l=-kernel_radius;l<=kernel_radius;l++)
			{
				for(int m=-kernel_radius;m<=kernel_radius;m++)
				{
					s_map_image[threadIdx.x][threadIdx.y]+=(s_image[threadIdx.x + 3 +l][threadIdx.y + 3 +m])*(d_Kernel[ map*kernel_size + (l+kernel_radius)*(kernel_length) +(m+kernel_radius)]);
					wt+=(d_Kernel[ map*kernel_size + (l+kernel_radius)*(kernel_length) +(m+kernel_radius)]);

				}
			}
			__syncthreads();
//			wt=2;
			s_map_image[threadIdx.x][threadIdx.y]=(s_map_image[threadIdx.x][threadIdx.y])/wt;
			__syncthreads();
	
			//Do dilate
			if((threadIdx.x%2) == 0)
			{
				if( s_map_image[threadIdx.x][threadIdx.y] > s_map_image[threadIdx.x+1][threadIdx.y])
					s_map_image[threadIdx.x+1][threadIdx.y]	= s_map_image[threadIdx.x][threadIdx.y];
				else
					s_map_image[threadIdx.x][threadIdx.y]	= s_map_image[threadIdx.x+1][threadIdx.y];
			}	
			__syncthreads();
			if((threadIdx.y%2) == 0)
			{
				if( s_map_image[threadIdx.x][threadIdx.y] > s_map_image[threadIdx.x][threadIdx.y+1])
					s_map_image[threadIdx.x][threadIdx.y+1]	= s_map_image[threadIdx.x][threadIdx.y];
				else
					s_map_image[threadIdx.x][threadIdx.y]	= s_map_image[threadIdx.x][threadIdx.y+1];
			}		
			__syncthreads();

			//Resize
			if(threadIdx.x%2==0 && threadIdx.y%2==0){
				d_output_l1[map*(l1_image_size) +(l1_image_width)*(y/2) + (x/2)]=s_map_image[threadIdx.x][threadIdx.y];

			}
			__syncthreads();


			s_image[threadIdx.x][threadIdx.y]=0;
			__syncthreads();
			s_image[threadIdx.x+6][threadIdx.y]=0;
			__syncthreads();
			s_image[threadIdx.x][threadIdx.y+6]=0;
			__syncthreads();
			s_image[threadIdx.x+6][threadIdx.y+6]=0;
			__syncthreads();

			s_map_image[threadIdx.x][threadIdx.y]=0;
			__syncthreads();
			//copy block to shared memory - ghost included
			s_image[threadIdx.x][threadIdx.y]=get_image_value(x-3,y-3,image_width,image_height,d_input);
			__syncthreads();
			s_image[threadIdx.x+6][threadIdx.y]=get_image_value(x+3,y-3,image_width,image_height,d_input);
			__syncthreads();
			s_image[threadIdx.x][threadIdx.y+6]=get_image_value(x-3,y+3,image_width,image_height,d_input);
			__syncthreads();
			s_image[threadIdx.x+6][threadIdx.y+6]=get_image_value(x+3,y+3,image_width,image_height,d_input);
			__syncthreads();
			printf("Manoj Venkat Testing");
		}
	}	
}


//Add weighted kernel
	__global__ void l2_addwt(
		float *d_output_l1,		// data coming from L1 stage
		float *d_inter_l2,		// will generate data to this intermediate 
		int *d_rand_l1_l2,
		int l2_image_width,
		int l2_image_height,
		int l1_l2_connections
		)
{
	//Thread id
	int x=threadIdx.x + blockDim.x*blockIdx.x;
	int y=threadIdx.y + blockDim.y*blockIdx.y;

	//allocate shared memory
	int l2_image_size=l2_image_width*l2_image_height;
	if(x>=0 && x<(l2_image_width) && y>=0 && y<(l2_image_height))
	{
	//copy block to shared memory - ghost included
		int t=l2_image_size*d_rand_l1_l2[blockIdx.z*(l1_l2_connections + 1)] +  y*l2_image_width +  x;
		d_inter_l2[blockIdx.z*(l2_image_size) + y*l2_image_width + x] = d_output_l1[t];//[l2_image_size*d_rand_l1_l2[blockIdx.z*(l1_l2_connections + 1)] +  y*l2_image_width +  x];
		__syncthreads();
		for(int i=0;i<l1_l2_connections;i++)
		{
			t=l2_image_size*d_rand_l1_l2[blockIdx.z*(l1_l2_connections+1)+i] + y*l2_image_width + x;
			d_inter_l2[ blockIdx.z*l2_image_size +  y*l2_image_width+x]=0.5*d_inter_l2[blockIdx.z*l2_image_size + y*l2_image_width + x] + 0.5*d_output_l1[t];
			__syncthreads();
		}
	}
}

//Giant kernel for l2 --- Convolution, Dilate & Resize
	__global__ void giant_l2(
		float *d_inter_l2,
		float *d_output_l2,
		int l2_image_width,
		int l2_image_height,
		int kernel_radius,
		int kernel_size,
		int l2_inter_image_size
		)
{
	//Thread id
	int x=threadIdx.x + blockDim.x*blockIdx.x;
	int y=threadIdx.y + blockDim.y*blockIdx.y;
	int map=blockIdx.z;
	int kernel_length=kernel_radius*2 +1;

	//allocate shared memory
	__shared__ float s_image[38][38];
	__shared__ float s_map_image[32][32];

		s_image[threadIdx.x][threadIdx.y]=0;
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y]=0;
		__syncthreads();
		s_image[threadIdx.x][threadIdx.y+6]=0;
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y+6]=0;
		__syncthreads();


//	if(x>=0 && x<l2_image_width && y>=0 && y<l2_image_height)
	{
		//reset shared memory
		s_image[threadIdx.x][threadIdx.y]=0;
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y]=0;
		__syncthreads();
		s_image[threadIdx.x][threadIdx.y+6]=0;
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y+6]=0;
		__syncthreads();

		s_map_image[threadIdx.x][threadIdx.y]=0;
		__syncthreads();

		//copy block to shared memory - ghost included
		s_image[threadIdx.x][threadIdx.y]=get_image_value_l2(x-3,y-3,l2_image_width,l2_image_height,map,l2_inter_image_size,d_inter_l2);
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y]=get_image_value_l2(x+3,y-3,l2_image_width,l2_image_height,map,l2_inter_image_size,d_inter_l2);
		__syncthreads();
		s_image[threadIdx.x][threadIdx.y+6]=get_image_value_l2(x-3,y+3,l2_image_width,l2_image_height,map,l2_inter_image_size,d_inter_l2);
		__syncthreads();
		s_image[threadIdx.x+6][threadIdx.y+6]=get_image_value_l2(x+3,y+3,l2_image_width,l2_image_height,map,l2_inter_image_size,d_inter_l2);
		__syncthreads();

		//test code
//		d_output_l2[threadIdx.y*l2_image_width + threadIdx.x]=s_image[threadIdx.x+6][threadIdx.y];
		int wt=0;
		if(x>=0 && x<l2_image_width && y>=0 && y<l2_image_height)
		{
		//Convolution
		for(int l=-kernel_radius;l<=kernel_radius;l++)
		{
			for(int m=-kernel_radius;m<=kernel_radius;m++)
				{
				s_map_image[threadIdx.x][threadIdx.y]+=(s_image[threadIdx.x + 3 +m][threadIdx.y + 3 +l])*(d_Kernel_l2[ map*kernel_size + (l+kernel_radius)*(kernel_length) +(m+kernel_radius)]);
				wt+=(d_Kernel_l2[ map*kernel_size + (l+kernel_radius)*(kernel_length) +(m+kernel_radius)]);
				}
		}
		__syncthreads();
		s_map_image[threadIdx.x][threadIdx.y]=s_map_image[threadIdx.x][threadIdx.y]/wt;
			
		//Do dilate
		if((threadIdx.x%2) == 0)
		{
			if( s_map_image[threadIdx.x][threadIdx.y] > s_map_image[threadIdx.x+1][threadIdx.y])
				s_map_image[threadIdx.x+1][threadIdx.y]	= s_map_image[threadIdx.x][threadIdx.y];
			else
				s_map_image[threadIdx.x][threadIdx.y]	= s_map_image[threadIdx.x+1][threadIdx.y];
		}	
		__syncthreads();
		if((threadIdx.y%2) == 0)
		{
			if( s_map_image[threadIdx.x][threadIdx.y] > s_map_image[threadIdx.x][threadIdx.y+1])
				s_map_image[threadIdx.x][threadIdx.y+1]	= s_map_image[threadIdx.x][threadIdx.y];
			else
				s_map_image[threadIdx.x][threadIdx.y]	= s_map_image[threadIdx.x][threadIdx.y+1];
		}		
		__syncthreads();

		//Resize
		if(threadIdx.x%2==0 && threadIdx.y%2==0)
		{
		d_output_l2[map*(l2_inter_image_size/4) +(l2_image_width/2)*(y/2) + (x/2)]=s_map_image[threadIdx.x][threadIdx.y];
		}
		__syncthreads();
		}

	}
	//	d_output_l2[threadIdx.y*32 + threadIdx.x]=s_map_image[threadIdx.x][threadIdx.y];


}



//***************************** DEEP LEARNING *************************


extern "C" void	operation_L1(float  *d_input, float *d_output_l1, int max_rows, int max_cols, int kernel_length, int l1_maps)
{
	int n_row=(max_rows+31)/32;
	int n_col=(max_cols+31)/32;
	int l1_image_size=max_rows/2*max_cols/2;	

	//create 2 dimension grid
	dim3 grid_size;
	grid_size.x=n_row;
	grid_size.y=n_col;
	grid_size.z=1;//l1_maps;

	//create 2 dimension block
	dim3 block_size;
	block_size.x=32;
	block_size.y=32;
	//	     StopWatchInterface *hgTimer = NULL;
	//          sdkCreateTimer(&hgTimer);
	
	filter2d<<<grid_size,block_size>>>(
			d_input,
			d_output_l1,
			max_rows,
			max_cols,
			kernel_length,
			l1_maps,
			l1_image_size
			);
}


extern "C" void	operation_L2(float  *d_output_l1, float *d_inter_l2, float *d_output_l2,int *d_rand_l1_l2, int max_rows, int max_cols, int l1_l2_connections, int l2_maps, int kernel_length)
{
	int n_row=(max_rows+31)/32;
	int n_col=(max_cols+31)/32;
	int l2_image_width=max_cols/2;
	int l2_image_height=max_rows/2;
	int kernel_radius=kernel_length/2;
	int kernel_size=kernel_length*kernel_length;
	int l2_inter_image_size=l2_image_width*l2_image_height;
	//create 2 dimension grid
	dim3 grid_size;
	grid_size.x=n_row;
	grid_size.y=n_col;
	grid_size.z=l2_maps;

	//create 2 dimension block
	dim3 block_size;
	block_size.x=32;
	block_size.y=32;

	l2_addwt<<<grid_size,block_size>>>(
			d_output_l1,
			d_inter_l2,
			d_rand_l1_l2,
			l2_image_width,
			l2_image_height,
			l1_l2_connections
			);

	giant_l2<<<grid_size,block_size>>>(
			d_inter_l2,
			d_output_l2,
			l2_image_width,
			l2_image_height,
			kernel_radius,
			kernel_size,
			l2_inter_image_size
			);

}

