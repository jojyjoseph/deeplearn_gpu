#ifndef _CLASSIFY_H
#define _CLASSIFY_H
#include <stdint.h>
void classifier_out(uint8_t *input , float *output, int NO_INPUT_NEURONS , int NO_HIDDEN_NEURONS , int NO_OUTPUT_NEURONS, float *input_weights , float *hidden_weights , float *output_weights,float *input_bias,float *hidden_bias,float *output_bias);
float dot_product(float *input, float *weight, int length);
void neuron_layer(float *input, float * weight, float *bias, float *output, int input_per_neuron, int no_neurons);
void normalize_input(uint8_t *input, float *norm_out, int length);
#endif // _CLASSIFY_H
